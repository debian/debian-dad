all: dad dad.1

dad: dad.in
	sed 's/@version@/'$(shell dpkg-parsechangelog -S Version)'/' < $< > $@
	chmod +x dad

dad.1: dad
	help2man -N -n "update Debian package automatically" ./$< -o $@

clean:
	rm -f dad dad.1
