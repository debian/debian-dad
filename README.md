##Debian's Automated Developer (Dad)

# About
This program prepares updates for Debian packages automating repetitive
tasks of human Developers.

# Usage:
1. ```dad init <source package name> && cd <source package name>```
This steps clones the packaging repository or downloads the source package
and creates a packaging repository as a starting point for the updates to be
prepared.
2. ```dad update```
This step performs all the supported updates in the local copy of the
packaging repository making each update step as separate commits.
For performing only a subset of the supported updates please see
```dad --help```.

# Warning
```dad update``` may download and execute untrusted code from the Internet
as part of a new upstream release, patches from Debian's BTS or Launchpad, or
from other sources. Always run ```dad update``` in an environment where
running untrusted code won't cause problems.
The updates to the package may also incorporate harmful changes which pass
tests but can compromise systems when the updated package is installed thus
always review the changes before accepting them to the official Debian
package.


# Features:
 * Updating packages to new upstream versions refreshing patches
 * Updating symbols files using Debian buildd logs

# Roadmap
See [TODO](TODO)
